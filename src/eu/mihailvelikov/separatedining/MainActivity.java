package eu.mihailvelikov.separatedining;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class MainActivity extends SherlockFragmentActivity {

	static final int ID_DIALOG = 1;
	private static final int REQUEST_CODE = 2;
	private static SharedPreferences mPrefs;
	private static int mDay;
	private static int mMonth;
	private static int mYear;
	public static GregorianCalendar mStartDate;
	// protected AdView adView;
	// public static GregorianCalendar mEndDate;

	private final int MENU_RESET = 0;
	private final int MENU_INFO = 1;
	private final int MENU_ABOUT = 2;
	private final int MENU_SHARE = 4;
	private Locale locale;
	private final static int MENU_SETTINGS = 3;

	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(0, MENU_RESET, 0, R.string.reset)
				// dd(R.string.reset)
				.setIcon(R.drawable.content_discard).setTitle(R.string.reset)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		menu.add(0, MENU_INFO, 0, R.string.info)
				.setIcon(R.drawable.action_about).setTitle(R.string.info)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		menu.add(0, MENU_SHARE, 0, R.string.share)
				.setIcon(R.drawable.social_share).setTitle(R.string.share)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		menu.add(0, MENU_ABOUT, 0, R.string.about)
				.setIcon(R.drawable.social_person).setTitle(R.string.about)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		
		menu.add(0, MENU_SETTINGS, 0, R.string.settings_title)
				.setIcon(R.drawable.action_settings)
				.setTitle(R.string.settings_title)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_RESET:
			clearPreferences();
			return true;
		case MENU_INFO:
			startInfoActivity();
			return true;
		case MENU_ABOUT:
			startAboutActivity();
			return true;
		case MENU_SETTINGS:
			startSettingsAcitivity();
			return true;
		case MENU_SHARE:
			handleShareButton(getString(R.string.share), getString(R.string.share_app));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void handleShareButton(final String title, final String text) {
		// create the send intent
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);

		// set the type
		shareIntent.setType("text/plain");

		// add a subject
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, R.string.share_with);

		// build the body of the message to be shared
		String shareMessage = text;

		// add the message
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);

		// start the chooser for sharing
		startActivity(Intent.createChooser(shareIntent, title));
	}

	private void startSettingsAcitivity() {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
	}

	private void startAboutActivity() {
		Intent i = new Intent(this, AboutActivity.class);
		startActivity(i);
	}

	private void startInfoActivity() {
		// TODO Auto-generated method stub
		Intent i = new Intent(this, InfoActivity.class);
		startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		TimeZone.setDefault(TimeZone.getTimeZone("Europe/London"));
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		/*
		 * if (firstLoad()) { File dir =
		 * Environment.getExternalStorageDirectory(); File startDietDatFile =
		 * new File(dir, "/sdcard/separate.dining.dat"); Editor edit =
		 * mPrefs.edit(); edit.putBoolean("secondVisit", true); edit.commit(); }
		 */

		/*
		 * Configuration config =
		 * getBaseContext().getResources().getConfiguration();
		 * 
		 * String lang = mPrefs.getString("lng", ""); if (! "".equals(lang) && !
		 * config.locale.getLanguage().equals(lang)) { locale = new
		 * Locale(lang); Locale.setDefault(locale); config.locale = locale;
		 * getBaseContext().getResources().updateConfiguration(config,
		 * getBaseContext().getResources().getDisplayMetrics()); }
		 */
		File sdcard = Environment.getExternalStorageDirectory();
		File startDietDatFile = new File(sdcard, "separate.dining.dat");

		if (startDietDatFile.exists()) {

			StringBuilder text = new StringBuilder();

			try {
				BufferedReader br = new BufferedReader(new FileReader(
						startDietDatFile));
				String line;

				while ((line = br.readLine()) != null) {
					text.append(line);
				}
			} catch (IOException e) {
				Toast.makeText(this, R.string.error_reading_date,
						Toast.LENGTH_SHORT).show();
			}
			startDietDatFile.delete();
			// Timestamp fileDate = new
			// Timestamp(Long.parseLong(text.toString()));
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(Long.parseLong(text.toString()));
			mYear = cal.get(Calendar.YEAR);
			mMonth = cal.get(Calendar.MONTH);
			mDay = cal.get(Calendar.DAY_OF_MONTH);
			savePreferences(mYear, mMonth, mDay);
		} else if (!readPreferences()) {
			Intent intent = new Intent(this, CompatibilityPickerActivity.class);
			// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivityForResult(intent, REQUEST_CODE);
			// return;
		}
		// 16 oct
		mStartDate = new GregorianCalendar(mYear, mMonth, mDay);
		CalendarView.mStartDate = mStartDate;

		CalendarItem.setmStartDate(mStartDate);
		// CalendarAdapter.setEndDate(mEndDate);

		// savedInstanceState.putInt("day", mDay);

		android.support.v4.app.Fragment calendarView = new CalendarView();
		// CalendarView.setStartDate(mStartDate);

		android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager()
				.beginTransaction();
		ft.add(android.R.id.content, calendarView).commit();
		/*
		 * // request TEST ads to avoid being disabled for clicking your own ads
		 * AdRequest adRequest = new AdRequest();
		 * 
		 * // create a Banner Ad adView = new AdView(this, AdSize.BANNER,
		 * MY_PUBLISHER_ID); // call the main layout from xml LinearLayout
		 * mainLayout = (LinearLayout) findViewById(R.id.main_layout);
		 * 
		 * // add the Banner Ad to our main layout mainLayout.addView(adView);
		 * 
		 * // Initiate a request to load an ad in TEST mode. The test mode will
		 * // work only on emulators and your specific test device, the users
		 * will // get real ads. adView.loadAd(adRequest); //
		 * addListenerOnReset();
		 */
		// adView = (AdView) findViewById(R.id.ad);
		// adView.loadAd(new AdRequest());
	}

	private boolean firstLoad() {
		// mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (mPrefs.contains("secondVisit")) {
			return false;
		}
		return true;
	}

	protected boolean readPreferences() {
		// mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (mDay > 0 && mMonth > 0 && mYear > 0) {
			return true;
		} else if (mPrefs.contains("day") && mPrefs.contains("month")
				&& mPrefs.contains("year")) {
			mDay = mPrefs.getInt("day", 1);
			mMonth = mPrefs.getInt("month", 1);
			mYear = mPrefs.getInt("year", 1970);
			return true;
		} else {
			return false;
		}
	}

	/*
	 * private void showTips() { new AlertDialog.Builder(this)
	 * .setIcon(R.drawable.ic_launcher) .setTitle(R.string.hello)
	 * .setMessage(R.string.tip) .setPositiveButton(R.string.ok, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int arg1) {
	 * dialog.cancel(); } }).show();
	 * 
	 * }
	 */

	protected void clearPreferences() {
		new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_launcher)
				.setTitle(R.string.confirm_clear_preferences_title)
				.setMessage(R.string.new_diet_start)
				.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int arg1) {
								Editor prefEditor = MainActivity.mPrefs.edit();
								prefEditor.clear();
								prefEditor.commit();
								mDay = mYear = mMonth = 0;
								Intent main = new Intent(MainActivity.this,
										MainActivity.class);
								main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(main);
								finish();
							}

						}).setNegativeButton(R.string.no, null).show();

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
			Bundle extras = data.getExtras();

			mYear = extras.getInt("year");
			mMonth = extras.getInt("month");
			mDay = extras.getInt("day");
			savePreferences(mYear, mMonth, mDay);

		} else {
			Toast.makeText(this, "No date was selected!", Toast.LENGTH_LONG)
					.show();
		}
	}

	protected void savePreferences(int year, int month, int day) {
		Editor edit = mPrefs.edit();
		edit.putInt("year", mYear);
		edit.putInt("month", mMonth);
		edit.putInt("day", mDay);
		edit.commit();

		finish();
		startActivity(getIntent());
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (locale != null) {
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getBaseContext().getResources().updateConfiguration(newConfig,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}
	// @Override
	// public void onDestroy() {
	// if (adView != null) {
	// adView.destroy();
	// }
	// super.onDestroy();
	// }
}
