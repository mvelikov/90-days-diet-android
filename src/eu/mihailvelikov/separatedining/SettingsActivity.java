package eu.mihailvelikov.separatedining;

import java.util.Locale;

import android.R.integer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class SettingsActivity extends SecondaryActivity {

	private AdView adView;
	private final int LNG_EN = R.id.lng_en;
	private final int LNG_BG = R.id.lng_bg;
	private final int LNG_RADIO_GROUP = R.id.lng_radio_group;
	private final int SUBMIT_SETTINGS_BTN = R.id.submit_settings_btn;
	private RadioGroup mRadioLngGroup;
	private Button mBtnSubmit;
	private RadioButton mRadioLngButton;
	private String mLng;

	private SharedPreferences mPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		adView = (AdView) findViewById(R.id.ad);
		adView.loadAd(new AdRequest());

		addListenerOnButton();
		mLng = Locale.getDefault().getLanguage();
		RadioButton radio;
		if ("bg".equals(mLng)) {
			radio = (RadioButton) findViewById(LNG_BG);
		} else {
			radio = (RadioButton) findViewById(LNG_EN);
		}
		radio.setChecked(true);
	}

	private void addListenerOnButton() {
		mRadioLngGroup = (RadioGroup) findViewById(LNG_RADIO_GROUP);
		mBtnSubmit = (Button) findViewById(SUBMIT_SETTINGS_BTN);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mBtnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int selectedRadio = mRadioLngGroup.getCheckedRadioButtonId();
				if (selectedRadio != -1) {
					mBtnSubmit = (RadioButton) findViewById(selectedRadio);

					String lng = "";
					switch (selectedRadio) {
					case LNG_BG:
						lng = "bg";
						break;
					case LNG_EN:
						lng = "en";
						break;
					default:
						break;
					}
					mLng = Locale.getDefault().getLanguage();
					if (!lng.equals(mLng)) {
						
						Editor edit = mPrefs.edit();

						edit.putString("lng", lng);
						edit.commit();
						
						final Resources res = SettingsActivity.this.getResources();
						Configuration config = getBaseContext().getResources().getConfiguration();
						Locale locale = new Locale(lng);
			            Locale.setDefault(locale);
			            config.locale = locale;
			            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
			            
			            res.updateConfiguration(config, null);
						
			            MyApplication.setLocale(locale);
			            Intent i = new Intent(SettingsActivity.this, MainActivity.class);
			            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
//			            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						finish();
						
						startActivity(i);
					} else {
						Toast.makeText(SettingsActivity.this, R.string.language_selected,
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}

}
