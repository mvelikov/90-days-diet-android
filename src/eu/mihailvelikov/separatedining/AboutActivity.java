package eu.mihailvelikov.separatedining;


import android.os.Bundle;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class AboutActivity extends SecondaryActivity {
	

	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		adView = (AdView) findViewById(R.id.ad);
		adView.loadAd(new AdRequest());
		// TextView textView = (TextView) findViewById(R.id.about_text);
		// textView.setMovementMethod(LinkMovementMethod.getInstance());
		// String str = getString(R.string.about_text);
		// textView.setText(Html.fromHtml(str));
	}
}
