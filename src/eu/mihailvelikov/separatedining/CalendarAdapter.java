package eu.mihailvelikov.separatedining;

import java.util.Calendar;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CalendarAdapter extends BaseAdapter {
	
	
	private static final int FIRST_DAY_OF_WEEK = Calendar.MONDAY;
	private final Calendar calendar;
	private static CalendarItem today;
	private final CalendarItem selected;
	private final LayoutInflater inflater;

	private CalendarItem[] days;

	public CalendarAdapter(Context context, Calendar monthCalendar) {
		calendar = monthCalendar;
		setToday(new CalendarItem(monthCalendar));
		selected = new CalendarItem(monthCalendar);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return days.length;
	}

	@Override
	public Object getItem(int position) {
		return days[position];
	}

	@Override
	public long getItemId(int position) {
		final CalendarItem item = days[position];
		if (item != null) {
			return days[position].getId();
		}
		return -1;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (view == null) {
			view = inflater.inflate(R.layout.calendar_item, null);
		}
		final TextView dayView = (TextView) view.findViewById(R.id.date);
		final CalendarItem currentItem = days[position];

		if (currentItem == null) {
			dayView.setClickable(false);
			dayView.setFocusable(false);
			// view.setBackgroundDrawable(null);
			dayView.setText(null);
		} else {
			view.setBackgroundResource(currentItem.getView());
			
			if (!currentItem.isClickable()){
				view.setBackgroundResource(R.drawable.normal_background);
			}
			view.setClickable(!currentItem.isClickable());

			dayView.setTextColor(Color.parseColor(currentItem.getColor()));
			dayView.setText(currentItem.getText());
		}

		return view;
	}

	public final void setSelected(int year, int month, int day) {
		selected.setYear(year);
		selected.setMonth(month);
		selected.setDay(day);
		notifyDataSetChanged();
	}

	public void refreshDays() {
		final int year = calendar.get(Calendar.YEAR);
		final int month = calendar.get(Calendar.MONTH);
		final int firstDayOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
		final int lastDayOfMonth = calendar
				.getActualMaximum(Calendar.DAY_OF_MONTH);
		final int blankies;
		final CalendarItem[] days;

		if (firstDayOfMonth == FIRST_DAY_OF_WEEK) {
			blankies = 0;
		} else if (firstDayOfMonth < FIRST_DAY_OF_WEEK) {
			blankies = Calendar.SATURDAY - (FIRST_DAY_OF_WEEK - 1);
		} else {
			blankies = firstDayOfMonth - FIRST_DAY_OF_WEEK;
		}
		days = new CalendarItem[lastDayOfMonth + blankies];

		for (int day = 1, position = blankies; position < days.length; position++) {
			days[position] = new CalendarItem(year, month, day++);
		}

		this.days = days;
		notifyDataSetChanged();
	}

	/**
	 * @return the today
	 */
	public static CalendarItem getToday() {
		return today;
	}

	/**
	 * @param today the today to set
	 */
	public static void setToday(CalendarItem today) {
		CalendarAdapter.today = today;
	}

}