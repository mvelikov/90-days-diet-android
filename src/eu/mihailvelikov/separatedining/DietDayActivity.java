package eu.mihailvelikov.separatedining;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class DietDayActivity extends SecondaryActivity {

	protected int mYear;
	protected int mMonth;
	protected int mDay;
	protected int mStartDay;
	protected int mStartMonth;
	protected int mStartYear;
	protected AdView adView;
	private CalendarItem calendarItem;

	// protected GregorianCalendar mStartDate;

	// protected GregorianCalendar mEndDate;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.diet_day);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		Calendar calendar = Calendar.getInstance();
		Intent intent = getIntent();

		final int mYear = intent.getIntExtra("selectedYear",
				calendar.get(Calendar.YEAR));
		final int mMonth = intent.getIntExtra("selectedMonth",
				calendar.get(Calendar.MONTH));
		final int mDay = intent.getIntExtra("selectedDay",
				calendar.get(Calendar.DATE));

		// final int mStartDay = intent.getIntExtra("startDay", 1);
		// final int mStartMonth = intent.getIntExtra("startMonth", 1);
		// final int mStartYear = intent.getIntExtra("startYear", 1970);
		try {
			calendarItem = new CalendarItem(mYear, mMonth, mDay);

			TextView viewDinnerText = (TextView) findViewById(R.id.dinner_text);

			String[] rulesArray = getResources().getStringArray(
					R.array.rules_list);
			String[] dietArray = getResources().getStringArray(
					calendarItem.getDietArray());
			int countDietArray = dietArray.length;
			TextView viewTipText = (TextView) findViewById(R.id.tip_text);
			TextView viewTipTitle = (TextView) findViewById(R.id.tip_title);
			TextView viewLunchText = (TextView) findViewById(R.id.lunch_text);
			TextView viewDayTitle = (TextView) findViewById(R.id.day_title);
			TextView viewTitle = (TextView) findViewById(R.id.title);
			TextView viewBreakfastTitle = (TextView) findViewById(R.id.breakfast_title);
			TextView viewDinnerTitle = (TextView) findViewById(R.id.dinner_title);

			TextView viewBreakfastText = (TextView) findViewById(R.id.breakfast_text);

			TextView viewLunchTitle = (TextView) findViewById(R.id.lunch_title);
			String titleStr = getString(R.string.day);
			String title = String.format(titleStr,
					calendarItem.getDietDay() + 1);
			viewTitle.setText(title);
			
			adView = (AdView) findViewById(R.id.ad);
			adView.loadAd(new AdRequest());
			ImageView mealView = (ImageView) findViewById(R.id.meal_img);
			try {
				// loads details for current days
				
				viewDayTitle.setText(calendarItem.getTitle());
				int dietColor = Color.parseColor(calendarItem.getColor());
				viewTipTitle.setTextColor(dietColor);
				viewDayTitle.setTextColor(dietColor);
				viewLunchTitle.setTextColor(dietColor);
				viewDinnerTitle.setTextColor(dietColor);
				viewBreakfastTitle.setTextColor(dietColor);
				viewTipText.setText(rulesArray[calendarItem.getTip()]);
				viewBreakfastText.setText(calendarItem.getBreakfast());
				int rand = calendarItem.getDay() * Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
				viewLunchText
						.setText(dietArray[calendarItem.getDay() * Calendar.getInstance().get(Calendar.DAY_OF_MONTH) % countDietArray]);
				viewDinnerText
						.setText(dietArray[calendarItem.getDay()
								* Calendar.getInstance().get(
										Calendar.DAY_OF_WEEK_IN_MONTH)
								% countDietArray]);// ((int) (Math.random() *
													// 100) % countDietArray)
				// viewDinnerText.setText(dinnerArray[0]);

				 mealView.setImageResource(calendarItem.getImage());
			} catch (Exception e) {
				Toast.makeText(this, R.string.invalid_diet_day,
						Toast.LENGTH_LONG).show();
				finish();
			}
		} catch (Exception e) {
			Toast.makeText(this, R.string.invalid_diet_day, Toast.LENGTH_LONG)
					.show();
			finish();
		}
	}

	private static int getStringIdentifier(Context context, String name,
			String needle) {
		return context.getResources().getIdentifier(name, needle,
				context.getPackageName());
	}

	public long daysBetween(GregorianCalendar startDate,
			GregorianCalendar endDate) {
		Calendar sDate = getDatePart(startDate);
		Calendar eDate = getDatePart(endDate);

		long daysBetween = 0;
		while (sDate.before(eDate)) {
			sDate.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}

	public static Calendar getDatePart(GregorianCalendar date) {
		Calendar cal = Calendar.getInstance(); // get calendar instance
		cal.setTimeInMillis(date.getTimeInMillis());
		cal.set(Calendar.HOUR_OF_DAY, 0); // set hour to midnight
		cal.set(Calendar.MINUTE, 0); // set minute in hour
		cal.set(Calendar.SECOND, 0); // set second in minute
		cal.set(Calendar.MILLISECOND, 0); // set millisecond in second

		return cal; // return the date part
	}
}