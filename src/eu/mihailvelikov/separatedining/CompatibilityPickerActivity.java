package eu.mihailvelikov.separatedining;



import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class CompatibilityPickerActivity extends FragmentActivity/* implements
		DatePickerDialog.OnDateSetListener*/ {
	private int mDay; 
	private int mMonth; // August, month starts from 0
	private int mYear;
	private int mYearSelected;
	private int mMonthSelected;
	private int mDaySelected;
	private DatePickerFragment mDatePicker;


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*GregorianCalendar calendar = new GregorianCalendar();
		mDay = calendar.get(Calendar.DATE);
		mMonth = calendar.get(Calendar.MONTH);
		mYear = calendar.get(Calendar.YEAR);*/
		final Calendar c = Calendar.getInstance();
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mMonth = c.get(Calendar.MONTH);
		mYear = c.get(Calendar.YEAR);
		/** Creating a bundle object to pass currently set date to the fragment */
		Bundle b = new Bundle();
		
		/** Adding currently set day to bundle object */
		b.putInt("set_day", mDay);
		
		/** Adding currently set month to bundle object */
		b.putInt("set_month", mMonth);
		
		/** Adding currently set year to bundle object */
		b.putInt("set_year", mYear);
		
		CompatibilityPickerDialogFragment datePicker = new CompatibilityPickerDialogFragment(mHandler);
		
		/** Setting the bundle object on datepicker fragment */
		datePicker.setArguments(b);				
		
		/** Getting fragment manger for this activity */
		FragmentManager fm = getSupportFragmentManager();				
		
		/** Starting a fragment transaction */
		FragmentTransaction ft = fm.beginTransaction();
		
		/** Adding the fragment object to the fragment transaction */
		ft.add(datePicker, "date_picker");
		
		/** Opening the DatePicker fragment */
		ft.commit();
		/*
		mDatePicker = new DatePickerFragment();
		mDatePicker.show(getSupportFragmentManager(), "datePicker");*/

	}
	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message m) {
			/**
			 * Creating a bundle object to pass currently set date to the
			 * fragment
			 */
			Bundle b = m.getData();

			/** Getting the day of month from bundle */
			mDay = b.getInt("set_day");

			/** Getting the month of year from bundle */
			mMonth = b.getInt("set_month");

			/** Getting the year from bundle */
			mYear = b.getInt("set_year");

			/**
			 * Displaying a short time message containing date set by Date
			 * picker dialog fragment
			 */
			finish();
		}
	};

	public void finish() {
		Intent data = new Intent();

		/*data.putExtra("year", mYearSelected);
		data.putExtra("month", mMonthSelected);
		data.putExtra("day", mDaySelected);*/
		data.putExtra("year", mYear);
		data.putExtra("month", mMonth);
		data.putExtra("day", mDay);
		setResult(RESULT_OK, data);
		super.finish();
	}

	/*public void onDateSet(DatePicker view, int year, int month, int day) {

		mYearSelected = year;
		mMonthSelected = month;
		mDaySelected = day;

		finish();
	}*/
}
