package eu.mihailvelikov.separatedining;

import java.util.Calendar;
import java.util.GregorianCalendar;



public class CalendarItem {
	
	//	private static final int WATER_DAY_VIEW = R.drawable.water_day;
//	private static final int PROTEIN_DAY_VIEW = R.drawable.protein_day;
//	private static final int BEANS_DAY_VIEW = R.drawable.beans_day;
//	private static final int FRUIT_DAY_VIEW = R.drawable.fruit_day;
//	private static final int CARBOHYDRATE_DAY_VIEW = R.drawable.carbohydrate_day;
	private static final int WATER_DAY_VIEW = R.drawable.normal_background;
	private static final int PROTEIN_DAY_VIEW = R.drawable.normal_background;
	private static final int BEANS_DAY_VIEW = R.drawable.normal_background;
	private static final int FRUIT_DAY_VIEW = R.drawable.normal_background;
	private static final int CARBOHYDRATE_DAY_VIEW = R.drawable.normal_background;
	private static final int DEFAULT_DAY_VIEW = R.drawable.normal_background;
	private static final int TODAY_VIEW = R.drawable.today_diet_day_background;
	
	private static final String WATER_DAY_COLOR = "#FF66FFFF";
	private static final String PROTEIN_DAY_COLOR = "#FF002AFF"; //#FF720303
	private static final String BEANS_DAY_COLOR = "#FF019B01";
	private static final String FRUIT_DAY_COLOR = "#FFFFA500";
	private static final String CARBOHYDRATE_DAY_COLOR = "#FFFF0000";
	private static final String DEFALT_DAY_COLOR = "black";
	
	private static final int WATER_DAY_TITLE = R.string.water_day;
	private static final int PROTEIN_DAY_TITLE = R.string.protein_day;
	private static final int BEANS_DAY_TITLE = R.string.beans_day;
	private static final int FRUIT_DAY_TITLE = R.string.fruit_day;
	private static final int CARBOHYDRATE_DAY_TITLE = R.string.carbohydrate_day;
	
	private static final int PROTEIN_ARRAY = R.array.protein_array;
	private static final int BEANS_ARRAY = R.array.beans_array;
	private static final int CARBOHYDRATE_ARRAY = R.array.carbohydrate_array;
	private static final int FRUIT_ARRAY = R.array.fruit_array;
	private static final int WATER_ARRAY = R.array.water_array;
	
	private static final int WATER_DAY_IMG = R.drawable.water_day_img;
	private static final int BEANS_DAY_IMG = R.drawable.beans_day_img;
	private static final int CARBOHYDRATE_DAY_IMG = R.drawable.carbohydrate_day_img;
	private static final int FRUIT_DAY_IMG = R.drawable.fruit_day_img;
	private static final int PROTEIN_DAY_IMG = R.drawable.protein_day_img;
	
	private static final int DIET_DAY_BREAKFAST = R.string.day_breakfast;
	
	private static final int RULES_COUNT = 13; 
	private int year;
	private int month;
	private int day;
	private final String text;
	private final long id;
	private final GregorianCalendar date;
	private final boolean isDietDay;
	private static GregorianCalendar mStartDate;
	private int view = DEFAULT_DAY_VIEW;
	private int title;
	private String color = DEFALT_DAY_COLOR; 
	private int dietDay;
	private boolean clickable = false;
	private int tip;
	private int dietArray = 0;
	private int breakfast = DIET_DAY_BREAKFAST;
	private int image = BEANS_DAY_IMG;

	public CalendarItem(int year, int month, int day) {
		this.setYear(year);
		this.setMonth(month);
		this.setDay(day);
		this.text = String.valueOf(day);
		this.id = Long.valueOf(year + "" + month + "" + day);
		this.date = new GregorianCalendar(year, month, day);
		this.setDietDay(getDaysDifference());
		this.isDietDay = isDietDay();
		setDietDayProperties();
	}

	private void setDietDayProperties() {
		
		setTip(dietDay % RULES_COUNT);
		int dietDayCounter = dietDay % 180;
		if (isDietDay && dietDayCounter >= 0 && dietDayCounter < 90) {
			if (dietDayCounter > 28) {
				dietDayCounter += 3;
			}
			if (dietDayCounter > 2 * 29 + 2) {
				dietDayCounter += 3;
			}
			if (dietDayCounter > 3 * 29 + 5) {
				dietDayCounter += 3;
			}

			if (dietDayCounter == 28 || dietDayCounter == 2 * 29 + 2 || dietDayCounter == 3 * 29 + 5) {
				this.setView(WATER_DAY_VIEW);
				setTitle(WATER_DAY_TITLE);
				setClickable(true);
				setColor(WATER_DAY_COLOR);
				setDietArray(WATER_ARRAY);
				setBreakfast(R.string.water_day_menu);
				setImage(WATER_DAY_IMG);
			} else if (dietDayCounter % 4 == 0) {
				setView(PROTEIN_DAY_VIEW);
				setTitle(PROTEIN_DAY_TITLE);
				setColor(PROTEIN_DAY_COLOR);
				setClickable(true);
				setDietArray(PROTEIN_ARRAY);
				setImage(PROTEIN_DAY_IMG);
			} else if (dietDayCounter % 4 == 1) {
				setView(BEANS_DAY_VIEW);
				setTitle(BEANS_DAY_TITLE);
				setColor(BEANS_DAY_COLOR);
				setClickable(true);
				setDietArray(BEANS_ARRAY);
				setImage(BEANS_DAY_IMG);
			} else if (dietDayCounter % 4 == 2) {
				setView(CARBOHYDRATE_DAY_VIEW);
				setTitle(CARBOHYDRATE_DAY_TITLE);
				setColor(CARBOHYDRATE_DAY_COLOR);
				setDietArray(CARBOHYDRATE_ARRAY);
				setClickable(true);
				setImage(CARBOHYDRATE_DAY_IMG);
			} else if (dietDayCounter % 4 == 3) {
				setDietArray(FRUIT_ARRAY);
				setColor(FRUIT_DAY_COLOR);
				setView(FRUIT_DAY_VIEW);
				setTitle(FRUIT_DAY_TITLE);
				setClickable(true);
				setImage(FRUIT_DAY_IMG);
			}
		}
		if (this.equals(CalendarAdapter.getToday()) /*&& this.getIsDietDay()*/) {
			setColor(DEFALT_DAY_COLOR);
			this.setView(TODAY_VIEW);
			setClickable(true);
		}
	}

	public CalendarItem(Calendar monthCalendar) {
		this(monthCalendar.get(Calendar.YEAR), monthCalendar
				.get(Calendar.MONTH), monthCalendar.get(Calendar.DAY_OF_MONTH));
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o instanceof CalendarItem) {
			final CalendarItem item = (CalendarItem) o;
			return item.getYear() == getYear() && item.getMonth() == getMonth() && item.getDay() == getDay();
		}
		return false;
	}

	/*
	 * protected boolean isDietDay() { GregorianCalendar g = new
	 * GregorianCalendar(year, month, day); //GregorianCalendar today = new
	 * GregorianCalendar(); return g.after(mStartDate) && g.before(mEndDate) ||
	 * g.equals(mStartDate); }
	 */
	public int getDaysDifference() {
		/**
		 * number of days that are between diet start date and compared to day
		 */
		long millis = date.getTimeInMillis() - getmStartDate().getTimeInMillis();
		return (int) Math.ceil((millis + 43200000) / 86400000);
	}

	protected boolean isDietDay() {

		int days = getDaysDifference();

		/**
		 * A day is part of the diet if it is after its start and if it is
		 * between 1st and 14th day of its start
		 */
		if (date.after(getmStartDate()) && days % 180 < 90
				|| date.equals(getmStartDate())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsDietDay() {
		return isDietDay;
	}

	/**
	 * @return the view
	 */
	public int getView() {
		return view;
	}

	/**
	 * @param view
	 *            the view to set
	 */
	public void setView(int view) {
		this.view = view;
	}

	/**
	 * @return the title
	 */
	public int getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(int title) {
		this.title = title;
	}

	/**
	 * @return the clickable
	 */
	public boolean isClickable() {
		return clickable;
	}

	/**
	 * @param clickable the clickable to set
	 */
	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the mStartDate
	 */
	public static GregorianCalendar getmStartDate() {
		return mStartDate;
	}

	/**
	 * @param mStartDate the mStartDate to set
	 */
	public static void setmStartDate(GregorianCalendar mStartDate) {
		CalendarItem.mStartDate = mStartDate;
	}

	/**
	 * @return the dietDay
	 */
	public int getDietDay() {
		return dietDay;
	}

	/**
	 * @param dietDay the dietDay to set
	 */
	public void setDietDay(int dietDay) {
		this.dietDay = dietDay;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param Color.parseColor(i) the color to set
	 */
	public void setColor(String i) {
		this.color = i;
	}

	/**
	 * @return the tip
	 */
	public int getTip() {
		return tip;
	}

	/**
	 * @param tip the tip to set
	 */
	public void setTip(int tip) {
		this.tip = tip;
	}

	/**
	 * @return the dietArray
	 */
	public int getDietArray() {
		return dietArray;
	}

	/**
	 * @param dietArray the dietArray to set
	 */
	public void setDietArray(int dietArray) {
		this.dietArray = dietArray;
	}

	/**
	 * @return the breakfast
	 */
	public int getBreakfast() {
		return breakfast;
	}

	/**
	 * @param breakfast the breakfast to set
	 */
	public void setBreakfast(int breakfast) {
		this.breakfast = breakfast;
	}

	/**
	 * @return the image
	 */
	public int getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(int image) {
		this.image = image;
	}
}
