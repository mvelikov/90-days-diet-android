package eu.mihailvelikov.separatedining;

import java.util.Locale;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

public class MyApplication extends Application {
	private static Locale locale = null;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (getLocale() != null) {
			newConfig.locale = getLocale();
			Locale.setDefault(getLocale());
			getBaseContext().getResources().updateConfiguration(newConfig,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		Configuration config = getBaseContext().getResources()
				.getConfiguration();

		String lang = settings.getString("lng", "");
		if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
			setLocale(new Locale(lang));
			Locale.setDefault(getLocale());
			config.locale = getLocale();
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

	/**
	 * @return the locale
	 */
	public static Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public static void setLocale(Locale locale) {
		MyApplication.locale = locale;
	}
}