package eu.mihailvelikov.separatedining;

import android.os.Bundle;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class InfoActivity extends SecondaryActivity {
	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		adView = (AdView) findViewById(R.id.ad);
		adView.loadAd(new AdRequest());
	}
}
